# Creative Coefficient - Challenge

## Challenge 1
- [Código Fuente](https://gitlab.com/LucasMontenegro1/creative-coefficient-challenge/-/tree/challenge_1?ref_type=heads)

## Challenge 2
- [Código Fuente](https://gitlab.com/LucasMontenegro1/creative-coefficient-challenge/-/tree/feature/queue-time?ref_type=heads)

## Challenge 3
- [Código Fuente](https://gitlab.com/LucasMontenegro1/creative-coefficient-challenge/-/tree/challenge_3?ref_type=heads)

## Challenge 4
- [Código Fuente](https://gitlab.com/LucasMontenegro1/creative-coefficient-challenge/-/tree/challenge_4?ref_type=heads)
